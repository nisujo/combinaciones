# Combinaciones

Script para generar todas las combinaciones posibles, como las combinaciones de un producto con variaciones de colores, tallas, etc.

```js
Product {
    variations: [
        ProductVariation {
            sku: '',
            stock: 0,
            price: 0,
            price_regular: 0,
            variation: [
                { title: 'Color', value: 'red' },
                { title: 'Material', value: 'Madera' },
                { title: 'Talla', value: 'S' }
            ],
            gallery: []
        }
    ]
}

```
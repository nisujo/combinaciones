// ecommerce /resources/js/admin/pages/product/partials/ProductAddVariation.vue

class ProductVariation {

    constructor(variation) {
        this.sku = ''
        this.stock = 0
        this.price = 0
        this.price_regular = 0
        this.variation = variation
        this.gallery = []
    }
}

var attributes = [{
        title: "Color",
        values: [
            "red", "blue", "yellow"
        ]
    },
    {
        title: "Material",
        values: [
            "Madera", "Metal", "Bronce"
        ]
    },
    {
        title: "Talla",
        values: [
            "S", "M", "L", "XL"
        ]
    },
];

let all = generateAllVariations(attributes);
console.log(all);
console.log(all[0]);
console.log("Cantidad de combinaciones: " + all.length);



/**
 * Generar todas las combinaciones de variaciones.
 */
function generateAllVariations(attributes) {
    let list = []
    let stocks = []

    attributes.forEach((option, index) => {

        list[index] = []

        option.values.forEach((value, vindex) => {

            list[index].push(JSON.stringify({
                title: option.title,
                value: value
            }))

        })

    })

    let combinations = getCombination(list)

    for (let i = 0; i < combinations.length; i++) {

        let variation = JSON.parse(`[${combinations[i]}]`)
        combinations[i] = new ProductVariation(variation)

    }


    // Filtrar solo las combinaciones que aún no existen
    let availableCombinations = combinations.filter(combination => {

        return !validateVariationExists(stocks, combination.variation)

    })

    return availableCombinations
}

/**
 * Generar combinación, recusivo.
 */
function getCombination(arr, pre) {
    pre = pre || ''

    if (!arr.length) {

        return pre

    }

    let ans = arr[0].reduce(function(ans, value) {

        let separator = pre == '' ? '' : ','

        return ans.concat(getCombination(arr.slice(1), pre + separator + value))

    }, [])

    return ans
}

/**
 * Validar si una variación ya está en uso.
 */
function validateVariationExists(stocks, variation) {
    let exists = false

    stocks.forEach(stock => {
        let count = 0

        stock.variation.forEach(stockVariation => {
            variation.forEach(variationItem => {

                if (variation.length == stock.variation.length &&
                    stockVariation.title == variationItem.title &&
                    stockVariation.value == variationItem.value) {

                    count++

                }

            })
        })

        if (count == variation.length) {
            exists = true
        }
    })

    return exists
}